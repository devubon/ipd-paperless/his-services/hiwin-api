import { Knex } from 'knex';

// import to his
export class ImportModels {

    /* บันทึก doctor order */

    /* รายการใบสั่งยา */
    savePrsc(db: Knex, data: any) {
        return db('prsc')
            .insert(data);
    }

    /* รายการยาและ วมย. */
    savePrscDt(db: Knex, data: any) {
        return db('prscdt')
            .insert(data);
    }

    /* บันทึกรายการสั่งยา */
    saveOrder(db: Knex, data: any) {
        return db('iprsc')
            .insert(data);
    }

    /* บันทึกรายการแลป */
    saveLab(db: Knex, data: any) {
        return db('lbbk')
            .insert(data);
    }

    /* บันทึกรายการ X-ray */
    saveXray(db: Knex, data: any) {
        return db('xryrqt')
            .insert(data);
    }

    /* บันทึกรายการหัตถการ */
    saveProcedure(db: Knex, data: any) {
        return db('ioprt')
            .insert(data);
    }

    /* บันทึกรายการค่าใช้จ่าย */
    saveCharge(db: Knex, data: any) {
        return db('incoth')
            .insert(data);
    }

    /* บันทึกรายการอาหาร */
    saveOrderFood(db: Knex, data: any) {
        return db('foodorder')
            .insert(data);
    }

    /* บันทึกรายการทันตกรรม */
    saveDental(db: Knex, data: any) {
        return db('dt')
            .insert(data);
    }

    /* บันทึกรายการหัตถการทันตกรรม */
    saveDentalProcedure(db: Knex, data: any) {
        return db('dtdx')
            .insert(data);
    }

    /* บันทึกรายการส่งต่อ */
    saveRefer(db: Knex, data: any) {
        return db('orfro')
            .insert(data);
    }

    /* บันทึกรายการแพทย์ */
    saveDiagnosis(db: Knex, data: any) {
        return db('iptdx')
            .insert(data);
    }

    /* บันทึกรายการ admit */
    saveAdmit(db: Knex, data: any) {
        return db('ipt')
            .insert(data);
    }

    /* บันทึกรายการ admit */
    saveAdmitRoom(db: Knex, data: any) {
        return db('iptadm')
            .insert(data);
    }

}