import { Knex } from 'knex';
export class ServicePaperlessModels {
  items(db: Knex,data:any) {
    return db('h_items')
    .insert(data).onConflict(['name','code','item_type_id']).merge();
  }

  insurance(db: Knex,data:any) {
    return db('h_insurance')
    .insert(data).onConflict(['name','code']).merge();
  }

  ward(db: Knex,data:any) {
    return db('h_ward')
    .insert(data).onConflict(['name','code']).merge();
  }

  department(db: Knex,data:any) {
    return db('h_department')
    .insert(data).onConflict(['name','code']).merge();
  }

  food(db: Knex,data:any) {
    return db('h_food')
    .insert(data).onConflict(['name','code']).merge();
  }
  bed(db: Knex,data:any) {
    return db('bed')
    .insert(data);
  }
  medicine(db: Knex,data:any) {
    return db('h_medicine')
    .insert(data).onConflict(['name','code']).merge();
  }

  medicine_usage(db: Knex,data:any) {
    return db('h_medicine_usage')
    .insert(data).onConflict(['name','description','code']).merge();
  }

  refer_cause(db: Knex,data:any) {
    return db('h_refer_cause')
    .insert(data).onConflict(['name','code']).merge();
  }

  refer_by(db: Knex,data:any) {
    return db('h_refer_by')
    .insert(data).onConflict(['name','code']).merge();
  }

  specialist(db: Knex,data:any) {
    return db('h_specialist')
    .insert(data).onConflict(['name','code']).merge();
  }
 
}