import { Knex } from 'knex';
export class ServiceModels {
  items(db: Knex) {
    let sql:any = db.raw(`
    select 
    labcode as code,
    replace(labname,"'",'') as name,
    1 as item_type_id,
    'EA' as unit,
    concat('[{"code":"TMLT","name":"',tmlt,'"},{"code":"CGD","name":"',cgd,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',pricelabcgd,'"},{"code":"UCS","name":"',pricelab,'"}]') as price
    from 
    lab
    union
    select
    xrycode  as code,
    replace(xryname,"'",'') as name,
    2 as item_type_id,
    'EA' as unit,
    concat('[{"code":"ICD9CM","name":"',icd9cm,'"},{"code":"CGD","name":"',cgd,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',pricexrycgd,'"},{"code":"UCS","name":"',pricexry,'"}]') as price
    FROM
    xray 
    union
    select 
    id as code,
    replace(nameprcd,"'",'') as name,
    cast(income as integer) as item_type_id,
    'EA' as unit,
    concat('[{"code":"ICD9CM","name":"',codeprcd,'"},{"code":"CGD","name":"',cgd,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',pricecgd,'"},{"code":"UCS","name":"',priceprcd,'"}]') as price
    from prcd 
    union
    select 
    meditem as code,
    replce(pres_nm,"'",'') as name,
    (case type when 1 then 8 when 2 then 11 when 3 then 8 when 4 then 14 when 5 then 10 when 6 then 15 end) as item_type_id,
    replace(pres_unt,"'",'') as unit,
    concat('[{"code":"',(case type when 1 then 'TMT' when 4 then 'INS' end),'","name":"',(case type when 1 then tmtid when 4 then meditem.codeinst end),'"},{"code":"STD24","name":"',stdcode,'"}]') as reference_code,
    concat('[{"code":"ALL","name":"',price,'"}]') as price
    from 
    meditem 
    union
    select 
    codecdt2 as code,
    replace(namecdt2,"'",'') as name,
    19 as item_type_id,
    'EA' as unit,
    concat('[{"code":"ICD10TM","name":"',icd9cm,'"},{"code":"CGD","name":"',cgd,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',pricecgd,'"},{"code":"UCS","name":"',pricedtt,'"}]') as price
    from 
    cdt2 
    union
    select 
    icd9cm as code,
    replace(icd9name,"'",'') as name,
    4 as item_type_id,
    'EA' as unit,
    concat('[{"code":"ICD9CM","name":"',icd9cm,'"}]') as reference_code,
    concat('[{"code":"ALL","name":"',price,'"}]') as price
    from 
    icd9cm2
    union
    select
    costcenter as code,
    namecost as name,
    cast(costcenter as integer) as item_type_id,
		'EA' as unit,
    concat('[{"code":"INCOME","name":"',costcenter,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',(case 
      when costcenter = '16' then 220
      when costcenter = '17' then 160
      when costcenter = '18' then 0
      when costcenter = '20' then 50
      when costcenter = '21' then 300
      when costcenter = '23' then 1100
      when costcenter = '05' then 50
    end),'"},{"code":"UCS","name":"',(case 
      when costcenter = '16' then 220
      when costcenter = '17' then 160
      when costcenter = '18' then 0
      when costcenter = '20' then 50
      when costcenter = '21' then 300
      when costcenter = '23' then 1100
      when costcenter = '05' then 100
    end),'"}]') as price
    from income 
    where costcenter in ('16','17','18','20','21','23','05')
    `);    
    return sql;
  }

  lab(db: Knex){
    let sql:any = db.raw(`
    select 
    labcode as code,
    replace(labname,"'",'') as name,
    1 as item_type_id,
    'EA' as unit,
    concat('[{"code":"TMLT","name":"',tmlt,'"},{"code":"CGD","name":"',cgd,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',pricelabcgd,'"},{"code":"UCS","name":"',pricelab,'"}]') as price
    from 
    lab`);
    return sql;
  }

  xray(db: Knex){
    let sql:any = db.raw(`
    select
    xrycode  as code,
    replace(xryname,"'",'') as name,
    2 as item_type_id,
    'EA' as unit,
    concat('[{"code":"ICD9CM","name":"',icd9cm,'"},{"code":"CGD","name":"',cgd,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',pricexrycgd,'"},{"code":"UCS","name":"',pricexry,'"}]') as price
    FROM
    xray 
    `);
    return sql; 
  }

  prcd(db: Knex){
    let sql:any = db.raw(`
    select 
    id as code,
    replace(nameprcd,"'",'') as name,
    cast(income as integer) as item_type_id,
    'EA' as unit,
    concat('[{"code":"ICD9CM","name":"',codeprcd,'"},{"code":"CGD","name":"',cgd,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',pricecgd,'"},{"code":"UCS","name":"',priceprcd,'"}]') as price
    from prcd 
    `);
    return sql; 
  }

  med(db: Knex){
    let sql:any = db.raw(`
    select 
    meditem as code,
    replace(pres_nm,"'",'') as name,
    (case type when 1 then 8 when 2 then 11 when 3 then 8 when 4 then 14 when 5 then 10 when 6 then 15 end) as item_type_id,
    replace(pres_unt,"'",'') as unit,
    concat('[{"code":"',(case type when 1 then 'TMT' when 4 then 'INS' end),'","name":"',(case type when 1 then tmtid when 4 then meditem.codeinst end),'"},{"code":"STD24","name":"',stdcode,'"}]') as reference_code,
    concat('[{"code":"ALL","name":"',price,'"}]') as price
    from 
    meditem     
    `);
    return sql; 
  }

  dental(db: Knex){
    let sql:any = db.raw(`
    select 
    codecdt2 as code,
    replace(namecdt2,"'",'') as name,
    19 as item_type_id,
    'EA' as unit,
    concat('[{"code":"ICD10TM","name":"',icd9cm,'"},{"code":"CGD","name":"',cgd,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',pricecgd,'"},{"code":"UCS","name":"',pricedtt,'"}]') as price
    from 
    cdt2     
    `);
    return sql; 
  }

  procedureOR(db: Knex){
    let sql:any = db.raw(`
    select 
    icd9cm as code,
    replace(icd9name,"'",'') as name,
    4 as item_type_id,
    'EA' as unit,
    concat('[{"code":"ICD9CM","name":"',icd9cm,'"}]') as reference_code,
    concat('[{"code":"ALL","name":"',price,'"}]') as price
    from 
    icd9cm2    
    `);
    return sql; 
  }

  income(db: Knex){
    let sql:any = db.raw(`
    select
    costcenter as code,
    namecost as name,
    cast(costcenter as integer) as item_type_id,
		'EA' as unit,
    concat('[{"code":"INCOME","name":"',costcenter,'"}]') as reference_code,
    concat('[{"code":"CGD","name":"',(case 
      when costcenter = '16' then 220
      when costcenter = '17' then 160
      when costcenter = '18' then 0
      when costcenter = '20' then 50
      when costcenter = '21' then 300
      when costcenter = '23' then 1100
      when costcenter = '05' then 50
    end),'"},{"code":"UCS","name":"',(case 
      when costcenter = '16' then 220
      when costcenter = '17' then 160
      when costcenter = '18' then 0
      when costcenter = '20' then 50
      when costcenter = '21' then 300
      when costcenter = '23' then 1100
      when costcenter = '05' then 100
    end),'"}]') as price
    from income 
    where costcenter in ('16','17','18','20','21','23','05')
    `);
    return sql; 
  }

  insurance(db: Knex) {
    let sql:any = db.raw(`
    select 
    pttype as code,
    replace(namepttype,"'",'') as name,
    concat('[{"code":"NHSO","name":"',instypeold,'"},{"code":"ECLAIM","name":"',stdcode,'"},{"code":"INSCL","name":"',inscl,'"}]') as reference_code
    from pttype;
    `);    
    return sql;
  }

  ward(db: Knex) {
    let sql:any = db.raw(`
    select 
    idpm as code,
    replace(nameidpm,"'",'') as name,
    bed as bed_amount
    from idpm
    `);    
    return sql;
  }

  department(db: Knex) {
    let sql:any = db.raw(`
    select 
    spclty as code,
    namespclty as name
    from spclty
    `);    
    return sql;
  }

  food(db: Knex) {
    let sql:any = db.raw(`select 
    codefood as code,
    replace(namefood,"'",'') as name
    from foodmain
    union
    SELECT
    codefoodsb as code,
    replace(namefoodsb,"'",'') as name
    from foodsub;`);    
    return sql;
  }
  bed(db: Knex) {
    let sql:any = db.raw(`select '' as code, '' as name`);    
    return sql;
  }
  medicine(db: Knex) {
    let sql:any = db.raw(`
    select 
    meditem as code,
    replace(pres_nm,"'",'') as name,
    productcat as product_cat,
    type as medicine_type_id,
    replace(pres_unt,"'",'') as unit,
    replace(strength,"'",'') as strenght,
    replace(medusage,"'",'') as default_usage,
    '' as default_route,
    if(ed = '1',true,false) as is_ed,
    null as provider_allow,
    if(pregnancy <> '',false,true) as pregnant_allow,
    replace(tallman,"'",'') as tallman
    from meditem
    `);    
    return sql;
  }

  medicine_usage(db: Knex) {
    let sql:any = db.raw(`
    select 
    dosecode as code,
    replace(dosename,"'",'') as name,
    concat(replace(doseprn1,"'",''),' ',replace(doseprn2,"'",'')) as description,
    null as time_per_day
    from 
    medusage
    `);    
    return sql;
  }

  refer_cause(db: Knex) {
    let sql:any = db.raw(`
    select 
    rfrcs as code,
    replace(namerfrcs,"'",'') as name
    from rfrcs
    `);    
    return sql;
  }

  refer_by(db: Knex) {
    let sql:any = db.raw(`
    select 
    codeloads as code,
    nameloads as name
    from l_loads
    `);    
    return sql;
  }

  specialist(db: Knex) {
    let sql:any = db.raw(`select spclty as code,namespclty as name from spclty;`);    
    return sql;
  }
 
}