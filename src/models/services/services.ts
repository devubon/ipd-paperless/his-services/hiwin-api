import { Knex } from "knex";
export class ServiceModels {
  /**
   * บันทึกข้อมูล Admit
   * @param db
   * @param data
   * @returns
   */

  /**
   * ข้อมูลทั่วไปผู้รับบริการ HIS
   * @param db
   * @param an
   * @param limit
   * @param offset
   * @returns
   */
  patient(
    db: Knex,
    an: string,
    limit: number,
    offset: number,
    dateStart: any,
    dateEnd: number
  ) {
    if (an) {
      let sql: any = db.raw(
        `
        select 
        cast(o.an as char(15)) as an,
        cast(p.hn as char(15)) as hn,
        p.pop_id as cid,
        if(p.pname ='', pl.prename,p.pname) as title,
        p.fname,
        p.lname,
        male.namemale as gender,
        p.brthdate as dob,
        concat(timestampdiff(year,brthdate,o.vstdttm),'-',
        mod(timestampdiff(month,brthdate,o.vstdttm),12),'-',
        if(day(brthdate) < day(o.vstdttm),day(o.vstdttm)-day(brthdate),day(last_day(subdate(o.vstdttm,interval 1 month)))+DAYOFMONTH(o.vstdttm)-DAYOFMONTH(brthdate))) as age,
        n.namentnlty as nationality,
        s.namentnlty as citizenship,
        r.namerlgn as religion,
        st.namemrt as married_status,
        j.nameoccptn as occupation,
        p.bloodgrp as blood_group,
        p.pttype as inscl,
        pttype.namepttype as inscl_name,
        concat(p.addrpart,' ',ifnull(m.namemoob,''),' ',ifnull(t.nametumb,''),' ',ifnull(a.nameampur,''),' ',ifnull(w.namechw,'')) as address,
        p.hometel as phone,
        o.preg as is_pregnant,
        infmname as reference_person_name,
        infmaddr as reference_person_address,
        infmtel as reference_person_phone,
        statusinfo as reference_person_relationship,
        date(o.vstdttm) as admit_date,
        time(o.vstdttm) as admit_time,
        x.icd10name as pre_diag,
        concat(l.prename,d.fname,' ',d.lname) as admit_by
        from 
        ovst as o 
        inner join 
        pt as p on o.hn=p.hn
        left join 
        male on p.male=male.male
        inner join ipt as i on o.an = i.an
        left join ntnlty as n on p.ntnlty = n.ntnlty
        left join ntnlty as s on p.ctzshp = s.ntnlty
        left join rlgn as r on p.rlgn = r.rlgn
        left join mrtlst as st on p.mrtlst = st.mrtlst
        left join pttype on p.pttype = pttype.pttype
        left join occptn as j on p.occptn=j.occptn
        left join changwat as w on p.chwpart=w.chwpart
        left join ampur as a on p.amppart=a.amppart and p.chwpart=a.chwpart
        left join tumbon as t on p.tmbpart=t.tmbpart and p.amppart=t.amppart and p.chwpart=t.chwpart
        left join mooban as m on p.moopart=m.moopart and p.tmbpart=m.tmbpart and p.amppart = m.amppart and p.chwpart=m.chwpart
        left join ovstdx as x on o.vn=x.vn and cnt=1
        left join dct as d on (case length(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        left join l_prename as l on d.pname = l.prename_code
        left join l_prename as pl on (CASE p.male 
          WHEN 1 THEN 
          IF(p.mrtlst < 6, 
            IF(timestampdiff(year,brthdate,o.vstdttm) < 15 , '001' , '003'),
            IF(timestampdiff(year,brthdate,o.vstdttm) < 20 , '832','831')
          )
          WHEN 2 THEN 
          IF(p.mrtlst = 1, 
            IF(timestampdiff(year,brthdate,o.vstdttm) < 15 , '002', '004'), 
            IF(p.mrtlst < 6 ,'005','863') 
          )
        END) = pl.prename_code
        where o.an <> 0 and i.dchdate = '0000-00-00' and o.an = ?
        order by o.an desc
      `,
        [an]
      );
      return sql;
    } else {
      let sql: any = db.raw(
        `
        select 
        cast(o.an as char(15)) as an,
        cast(p.hn as char(15)) as hn,
        p.pop_id as cid,
        if(p.pname ='', pl.prename,p.pname) as title,
        p.fname,
        p.lname,
        male.namemale as gender,
        p.brthdate as dob,
        concat(timestampdiff(year,brthdate,o.vstdttm),'-',
        mod(timestampdiff(month,brthdate,o.vstdttm),12),'-',
        if(day(brthdate) < day(o.vstdttm),day(o.vstdttm)-day(brthdate),day(last_day(subdate(o.vstdttm,interval 1 month)))+DAYOFMONTH(o.vstdttm)-DAYOFMONTH(brthdate))) as age,
        n.namentnlty as nationality,
        s.namentnlty as citizenship,
        r.namerlgn as religion,
        st.namemrt as married_status,
        j.nameoccptn as occupation,
        p.bloodgrp as blood_group,
        p.pttype as inscl,
        pttype.namepttype as inscl_name,
        concat(p.addrpart,' ',ifnull(m.namemoob,''),' ',ifnull(t.nametumb,''),' ',ifnull(a.nameampur,''),' ',ifnull(w.namechw,'')) as address,
        p.hometel as phone,
        o.preg as is_pregnant,
        infmname as reference_person_name,
        infmaddr as reference_person_address,
        infmtel as reference_person_phone,
        statusinfo as reference_person_relationship,
        date(o.vstdttm) as admit_date,
        time(o.vstdttm) as admit_time,
        x.icd10name as pre_diag,
        concat(l.prename,d.fname,' ',d.lname) as admit_by
        from 
        ovst as o 
        inner join 
        pt as p on o.hn=p.hn
        left join 
        male on p.male=male.male
        inner join ipt as i on o.an = i.an
        left join ntnlty as n on p.ntnlty = n.ntnlty
        left join ntnlty as s on p.ctzshp = s.ntnlty
        left join rlgn as r on p.rlgn = r.rlgn
        left join mrtlst as st on p.mrtlst = st.mrtlst
        left join pttype on p.pttype = pttype.pttype
        left join occptn as j on p.occptn=j.occptn
        left join changwat as w on p.chwpart=w.chwpart
        left join ampur as a on p.amppart=a.amppart and p.chwpart=a.chwpart
        left join tumbon as t on p.tmbpart=t.tmbpart and p.amppart=t.amppart and p.chwpart=t.chwpart
        left join mooban as m on p.moopart=m.moopart and p.tmbpart=m.tmbpart and p.amppart = m.amppart and p.chwpart=m.chwpart
        left join ovstdx as x on o.vn=x.vn and cnt=1
        left join dct as d on (case length(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
        left join l_prename as l on d.pname = l.prename_code
        left join l_prename as pl on (CASE p.male 
          WHEN 1 THEN 
          IF(p.mrtlst < 6, 
            IF(timestampdiff(year,brthdate,o.vstdttm) < 15 , '001' , '003'),
            IF(timestampdiff(year,brthdate,o.vstdttm) < 20 , '832','831')
          )
          WHEN 2 THEN 
          IF(p.mrtlst = 1, 
            IF(timestampdiff(year,brthdate,o.vstdttm) < 15 , '002', '004'), 
            IF(p.mrtlst < 6 ,'005','863') 
          )
        END) = pl.prename_code
        where date(i.rgtdate) between ? and ? and 
        o.an <> 0 and i.dchdate = '0000-00-00' 
        order by o.an desc
        `,
        [dateStart, dateEnd]
      );
      return sql;
    }
  }

  review(db: Knex, an: string) {
    let sql: any = db.raw(
      `
    select 
    o.an,
    o.vn,
    o.hn,
    cc.symptom as chief_complaint,
    (select group_concat(pillness) from pillness where o.vn=pillness.vn) as present_illness,
    (select group_concat(sign) from sign where o.vn = sign.vn) as physical_exam,
    (select group_concat(phistory) from phistory where o.vn =phistory.vn) as past_history,
    o.bw as body_weight,
    o.height as body_height,
    o.tt as body_temperature,
    o.waist_cm as waist,
    o.pr as pulse_rate,
    o.rr as respiratory_rate,
    o.sbp as systolic_blood_pressure,
    o.dbp as diatolic_blood_pressure,
    e.coma_eye as eye_score,
    e.coma_move as movement_score,
    e.coma_speak as verbal_score,
    null as oxygen_sat
    from 
    ovst as o 
    inner join ipt as i on o.an=i.an 
    left join emergency as e on o.vn=e.vn
    left join symptm as cc on o.vn=cc.vn
    where i.an = ?`,
      [an]
    );
    return sql;
  }

  treatement(db: Knex, an: string) {
    let sql: any = db.raw(
      `
    select 
    o.an,
    1 as item_type_id,
    l.labcode as item_code,
    b.labname as item_name,
    b.tmltname as item_describe,
    i.rcptamt as item_price,
    1 as quantity,
    l.srvdate as treatment_date,
    time(l.srvtime*100) as treatment_time,
    concat(lp.prename,d.fname,' ',d.lname) as treatment_by
    from 
    ovst as o 
    inner join 
    lbbk as l on o.vn = l.vn and l.an = 0
    left join lab as b on l.labcode = b.labcode
    left join incoth as i on l.ln = i.codecheck and i.vn=o.vn
    left join dct as d on (case length(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
    left join l_prename as lp on d.pname = lp.prename_code
    where o.an = ?
    union
    select 
    o.an,
    2 as item_type_id,
    l.xrycode as item_code,
    b.xryname as item_name,
    b.remark as item_describe,
    l.charge as item_price,
    l.qty as quantity,
    l.vstdate as treatment_date,
    time(l.vsttime*100) as treatment_time,
    concat(lp.prename,d.fname,' ',d.lname) as treatment_by
    from 
    ovst as o 
    inner join 
    xryrgt as l on o.vn = l.vn and l.an = 0
    left join xray as b on l.xrycode = b.xrycode
    left join dct as d on (case length(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
    left join l_prename as lp on d.pname = lp.prename_code
    where o.an = ?
    union
    select 
    o.an,
    cast(b.income as INT) as item_type_id,
    l.codeicd9id as item_code,
    b.nameprcd as item_name,
    b.fullname as item_describe,
    l.charge as item_price,
    l.qty as quantity,
    date(l.opdttm) as treatment_date,
    time(l.opdttm) as treatment_time,
    concat(lp.prename,d.fname,' ',d.lname) as treatment_by
    from 
    ovst as o 
    inner join 
    oprt as l on o.vn = l.vn and l.an = 0
    left join prcd as b on l.codeicd9id = b.id
    left join dct as d on (case length(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
    left join l_prename as lp on d.pname = lp.prename_code
    where o.an = ?
    union
    select 
    o.an,
    (case b.type when 1 then 8 when 2 then 11 when 3 then 8 when 4 then 14 when 5 then 8 when 6 then 15 end) as item_type_id,
    r.meditem as item_code,
    b.name as item_name,
    if(r.medusage='',x.doseprn,u.dosename) as item_describe,
    r.charge as item_price,
    r.qty as quantity,
    date(l.prscdate) as treatment_date,
    time(l.prsctime*100) as treatment_time,
    concat(lp.prename,d.fname,' ',d.lname) as treatment_by
    from 
    ovst as o 
    inner join 
    prsc as l on o.vn = l.vn and l.an = 0
    left join prscdt as r on l.prscno=r.prscno 
    left join meditem as b on r.meditem = b.meditem
    left join medusage as u on r.medusage = u.dosecode
    left join xdose as x on r.xdoseno = x.xdoseno
    left join dct as d on (case length(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
    left join l_prename as lp on d.pname = lp.prename_code
    where o.an = ?`,
      [an, an, an, an]
    );
    return sql;
  }

  admit(db: Knex, an: string) {
    let sql: any = db.raw(
      `
    select 
    o.an,o.hn,o.vn
    ,date_format(o.vstdttm,'%Y-%m-%d') as admit_date
    ,time(o.vstdttm) as admit_time
    ,concat(l.prename,d.fname,' ',d.lname) as admit_by
    ,i.dept as department_code
    ,i.ward as ward_code
    ,null as bed_code
    ,a.bedno as bed_number
    ,t.pttype as inscl_code
    ,t.namepttype as inscl_name
    ,i.prediag as pre_diag
    from ovst as o 
    inner join ipt as i on o.an=i.an 
    left join iptadm as a on i.an=a.an
    left join pttype as t on i.pttype=t.pttype
    left join dct as d on (case length(o.dct) when 5 then o.dct = d.lcno when 4 then substr(o.dct,3,2) = d.dct end)
    left join l_prename as l on d.pname = l.prename_code
    where o.an = ?`,
      [an]
    );
    return sql;
  }

  lab_result(db: Knex, an: string, items_code: string) {
    let sql: any = db.raw(
      `
    select 
    l.vn as vn,
    l.an as an,
    l.labcode as code,
    r.lab_name as name,
    r.lab_code_local as lab_test_name,
    r.labresult as lab_test_result,
    r.unit as lab_test_unit,
    r.normal as lab_test_ref_range,
    l.srvdate as lab_test_result_date,
    time(l.srvtime*100) as lab_test_result_time,
    l.reportby as lab_test_result_by
    from 
    lbbk as l 
    inner join ovst as o on o.vn=l.vn
    inner join labresult as r on l.ln=r.ln
    where o.an = ? and l.labcode = ?
    `,
      [an, items_code]
    );
    return sql;
  }

  lab_result_an(db: Knex, an: string) {
    let sql: any = db.raw(
      `
    select 
    l.vn as vn,
    l.an as an,
    l.labcode as code,
    r.lab_name as name,
    r.lab_code_local as lab_test_name,
    r.labresult as lab_test_result,
    r.unit as lab_test_unit,
    r.normal as lab_test_ref_range,
    l.srvdate as lab_test_result_date,
    time(l.srvtime*100) as lab_test_result_time,
    l.reportby as lab_test_result_by
    from 
    lbbk as l 
    inner join ovst as o on o.vn=l.vn
    inner join labresult as r on l.ln=r.ln
    where o.an = ?
    `,
      [an]
    );
    return sql;
  }

  lab_finding(db: Knex, an: string) {
    let sql: any = db.raw(
      `
    select 
    l.vn as vn,
    l.an as an,
    l.labcode as code,
    r.lab_name as name,
    r.lab_code_local as lab_test_name,
    r.labresult as lab_test_result,
    r.unit as lab_test_unit,
    r.normal as lab_test_ref_range,
    l.srvdate as lab_test_result_date,
    time(l.srvtime*100) as lab_test_result_time,
    l.reportby as lab_test_result_by
    from 
    lbbk as l 
    inner join ovst as o on o.vn=l.vn
    inner join labresult as r on l.ln=r.ln
    where o.an = ? and l.an = 0
    `,
      [an]
    );
    return sql;
  }
  
  xray_result(db: Knex, an: string) {
    let sql: any = db.raw(`
    select 
    o.an as an,
    l.xrycode as code,
    x.xryname as name,
    l.fread as result,
		l.xrydate as result_date,
		time(l.xrytime*100) as result_time,
		'' as result_by
    from xryrgt as l
    inner join xray as x on x.xrycode=l.xrycode
    inner join ovst as o on o.vn=l.vn
    where o.an = ?
    `, [an]);
    return sql;
  }

  ekg_file(db: Knex, an: string) {
    let sql: any = db.raw(`
    select
    i.an as an,
    i.codeicd9id as code,
    i.icd9name as name,
    '' as image_url
    from 
    ioprt as i
    where 
    i.icd9cm = '8952' and i.an = ?
    `, [an]);
    return sql;
  }

  patient_allergy(db: Knex, an: string) {
    let sql: any = db.raw(
      `
    select 
    o.an as an,
    namedrug as drug_name,
    alevel as allergy_level,
    detail as symptom,
    entrydate as allergy_date
    from allergy
    inner join ovst as o on o.hn=allergy.hn 
    where o.an = ?
    `,
      [an]
    );
    return sql;
  }
}
