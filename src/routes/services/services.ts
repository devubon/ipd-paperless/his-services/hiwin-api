import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _, { now } from 'lodash';
import * as luxon from 'luxon';

import { ServiceModels } from '../../models/services/services';
import patientSchema from '../../schema/service/service_patient';
import anSchema from '../../schema/service/service_an';

export default async (fastify: FastifyInstance, _options: any, done: any) => {
  const db = fastify.db;
  const serviceModels = new ServiceModels();

  fastify.get('/patient', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: patientSchema,
        // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const _query: any = request.query;
      // const { limit, offset, an ,dateStart , dateEnd} = _query;
      const _limit = _query.limit || 20;
      const _offset = _query.offset || 0;
      let _an:any;
      let _dateStart:any;
      let _dateEnd:any;
      console.log(_query);
      
      
      if(_query.an == undefined){
        _an = null
      }else{
        _an = _query.an
      }     
      if(_query.dateStart !== undefined || _query.dateStart !== ''){
        _dateStart = _query.dateStart;
      }else{
        _dateStart = null
      }      
      if(_query.dateEnd !== undefined || _query.dateEnd !== ''){
        _dateEnd = _query.dateEnd;
      }else{
        _dateEnd = null
      }
      //  = an || null;
      const data: any = await serviceModels.patient(db,_an,_limit,_offset,_dateStart,_dateEnd);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/review', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.review(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/treatement', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.treatement(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/admit', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      console.log('_an : ',_an);
      
      const data: any = await serviceModels.admit(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });
  
  fastify.get('/lab-result', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    // schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an,items_code } = _query;
      const _an = an || null;
      const _items_code = items_code || null;
      const data: any = await serviceModels.lab_result(db,_an,_items_code);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/lab-finding', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    // schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.lab_finding(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/xray-result', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    // schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.xray_result(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/ekg', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.ekg_file(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/patient-allergy', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: anSchema,
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const _query: any = request.query;
      const { an } = _query;
      const _an = an || null;
      const data: any = await serviceModels.patient_allergy(db,_an);
      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  fastify.get('/admit-views', {
    config: {
      rateLimit: {
        max: 10,
        timeWindow: '1 minute'
      }
    },
    schema: patientSchema,
        // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const _query: any = request.query;
      const { limit, offset, an ,dateStart , dateEnd} = _query;
      const _limit = limit || 20;
      const _offset = offset || 0;
      const _dateStart = dateStart || luxon.DateTime.now().toFormat('yyyy-MM-dd');
      const _dateEnd = dateEnd || luxon.DateTime.now().toFormat('yyyy-MM-dd');
      // const _dateEnd = dateEnd || luxon.DateTime.fromFormat(dateStart,'yyyy-MM-dd');
      const _an = an || null;
      const data: any = await serviceModels.patient(db,_an,_limit,_offset,_dateStart,_dateEnd);
        // console.log(data[0]);
        
      if(data[0].length > 0){
        for(let i of data[0]) {
          let review: any = await serviceModels.review(db,i.an);
          if(review[0].length > 0){i.review = review[0]}
    
          let treatement: any = await serviceModels.treatement(db,i.an);
          if(treatement[0].length > 0){i.treatement = treatement[0]}

          let admit: any = await serviceModels.admit(db,i.an);
          if(admit[0].length > 0){i.admit = admit[0]}

          let lab_result_an: any = await serviceModels.lab_result_an(db,i.an);
          if(lab_result_an[0].length > 0){i.lab_result = lab_result_an[0]}

          let lab_finding: any = await serviceModels.lab_finding(db,i.an);
          if(lab_finding[0].length > 0){i.lab_finding = lab_finding[0]}

          let xray_result: any = await serviceModels.xray_result(db,i.an);
          if(xray_result[0].length > 0){i.xray_result = xray_result[0]}

          let ekg: any = await serviceModels.ekg_file(db,i.an);
          if(ekg[0].length > 0){i.ekg = ekg[0]}

          let patient_allergy: any = await serviceModels.patient_allergy(db,i.an);
          if(patient_allergy[0].length > 0){i.patient_allergy = patient_allergy[0]}

        }    
      }

      return reply.status(StatusCodes.OK).send({
        status: 'ok',
        data:data[0]
      });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  done();
} 
