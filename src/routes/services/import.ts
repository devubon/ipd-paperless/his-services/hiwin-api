import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";
import { getReasonPhrase, StatusCodes } from "http-status-codes";
import _ from "lodash";
import { DateTime } from "luxon";

import { ImportModels } from "../../models/his_model/import";

export default async (fastify: FastifyInstance, _options: any, done: any) => {
  const db = fastify.db;
  const importModels = new ImportModels();

  // บันทึกรายการ doctor order
  fastify.post(
    "/save",
    {},
    async (request: FastifyRequest, reply: FastifyReply) => {
      const data: any = request.body;
      const admit = data.admit;
      const order = data.order;
      const food = data.food;

      try {
        // บันทึกรายการสั่งยา
        let prsc = {
          vn: admit.vn,
          an: admit.an,
          prscdate: order.doctor_order_date,
          prsctime: order.doctor_order_time,
          pttype: admit.pttype,
          charge: order.charge,
        };
        const prscno = await importModels.savePrsc(db, prsc);

        let orders :any = [];
        let lab :any = [];
        let xray :any = [];
        let procedure :any = [];
        let charge :any = [];
        for(let i of order){
            let _order_time : any = order.doctor_order_time.split(':');
            // บันทึกรายการยาและ วมย.
            if(i.item_type_id.includes(4,5,6,8,9,10,11,16)){
                let is_oneday : string = '0';
                if(i.order_type_id == 2){
                    is_oneday = '1';
                } else {
                    is_oneday = '0';
                }
    
                let off_date = '0000-00-00';
                let off_time = '0.0';
                if(i.medicine_usage_code == 'off'){
                    off_date = order.doctor_order_date;
                    off_time = _order_time[0] + _order_time[1] + '.0';
                }
    
                let info ={
                    prscno: prscno[0],
                    an: admit.an,
                    hn: admit.hn,
                    prscdate: order.doctor_order_date,
                    prsctime: order.doctor_order_time,
                    namedrug: i.item_name,
                    qty: i.quantity,
                    meditem: i.item_code,
                    medusage: i.medicine_usage_code,
                    lcontinue: is_oneday,
                    offdate: off_date,
                    offtime: off_time,
                }
                orders.push(info);    
            }
            // บันทึกรายการแลป
            if(i.item_type_id == 1){
                let info = {
                    labcode: i.item_code,
                    vn: admit.vn,
                    an: admit.an,
                    hn: admit.hn,
                    requestby: order.doctor_order_by_name,
                    vstdttm: new Date(order.doctor_order_date).toLocaleDateString('th-TH') + ' ' + order.doctor_order_time,
                    senddate: new Date().toLocaleDateString('th-TH'),
                    sendtime: _order_time[0] + _order_time[1],
                    pttype: admit.pttype,
                    finish: '0'
                }
                lab.push(info);
            }
            // บันทึกรายการ X-ray
            if(i.item_type_id == 2){
                let info = {
                    xrycode: i.item_code,
                    vn: admit.vn,
                    an: admit.an,
                    hn: admit.hn,
                    vstdate: new Date(order.doctor_order_date).toLocaleDateString('th-TH'),
                    vsttime: _order_time[0] + _order_time[1],
                    rqttime: _order_time[0] + _order_time[1],
                }
                xray.push(info);
            }
            // บันทึกรายการหัตถการ
            if(i.item_type_id.includes(3,12,18,19,20,21,22,26)){
                let info = {
                    codeicd9id: i.item_code,
                    icd9cm: i.item.std_code,
                    icd9name: i.item_name,
                    an: admit.an,
                    hn: admit.hn,
                    date: new Date(order.doctor_order_date).toLocaleDateString('th-TH'),
                    time: _order_time[0] + _order_time[1],
                    charge: i.item.charge,
                    qty: i.quantity,
                }
                procedure.push(info);
            }
            // บันทึกรายการค่าใช้จ่าย
            if(i.item.charge > 0 && i.item_type_id != 1 && i.item_type_id !=2){
                let info = {
                    an: admit.an,
                    vn: admit.vn,
                    date: new Date(order.doctor_order_date).toLocaleDateString('th-TH'),
                    time: _order_time[0] + _order_time[1],
                    income: i.item_type.code,
                    pttype: admit.pttype,
                    rcptamt: i.item.charge,
                    cgd: i.item.cgd,
                }
                charge.push(info);
            }
        }

        await importModels.saveOrder(db, orders);
        await importModels.saveLab(db, lab);
        await importModels.saveXray(db, xray);
        await importModels.saveProcedure(db, procedure);
        await importModels.saveCharge(db, charge);

        return reply.status(StatusCodes.OK).send({
          status: "ok"
        });
      } catch (error: any) {
        request.log.error(error);
        return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({
          status: "error",
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR),
        });
      }
    }
  );

  // บันทึกรายการ Refer
  
};
