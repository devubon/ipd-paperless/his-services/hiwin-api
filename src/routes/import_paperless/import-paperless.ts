import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import { DateTime } from 'luxon';

import { ServiceModels } from '../../models/services/services_lookup';
import { ServicePaperlessModels } from '../../models/import_paperless/import-paperless';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db = fastify.db;
  const pg = fastify.pg;
  const serviceModels = new ServiceModels();
  const servicePaperlessModels = new ServicePaperlessModels();
  //ข้อมูลไม่ออก
  fastify.get('/bed', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
        const data: any = await serviceModels.bed(db);        
        if(data[0].length > 0) {
          for(let i of data[0]){
            const info: any = await servicePaperlessModels.bed(pg,i);

          }
          return reply.status(StatusCodes.OK).send({
            status: 'ok',
            data:data
          });
        }else{
          return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
        }
      

    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว
  fastify.get('/department', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.department(db);
      if(data[0].length > 0) {
        for(let i of data[0]){
          const info: any = await servicePaperlessModels.department(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }
    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว
  fastify.get('/food', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.food(db);
      if(data[0].length > 0) {
        for(let i of data[0]){
          const info: any = await servicePaperlessModels.food(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }
    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว นำเข้าไม่ได้
  fastify.get('/insurance', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.insurance(db);

      if(data[0].length > 0) {
        let _data: any = [];

        // for (const i of data[0]) {
        //   let _reference_code = JSON.parse(i.reference_code);
          
        //   delete i.reference_code;
        //   i.reference_code = _reference_code;
  
          
        //   _data.push(i);

        // }
        for(let v of data[0]) {
          console.log(v);
          
          const info: any = await servicePaperlessModels.insurance(pg,v);

        }

        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }

    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว นำเข้าไม่ได้
  fastify.get('/items', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      // const data: any = await serviceModels.items(db);
      const lab: any = await serviceModels.lab(db);
      const xray: any = await serviceModels.xray(db);
      const medicine: any = await serviceModels.med(db);
      const prcd: any = await serviceModels.prcd(db);
      const dental:any = await serviceModels.dental(db);
      const procedureOR: any = await serviceModels.procedureOR(db);
      const income:any = await serviceModels.income(db);

      let _data: any = [];
      console.log("lab");
      for( const i of lab[0]){
        // let _reference_code = JSON.parse(i.reference_code);
        
        // delete i.reference_code;
        // i.reference_code = _reference_code;

        // let _price = JSON.parse(i.price);
        // delete i.price;
        // i.price = _price;
        _data.push(i);
      }
      console.log("xray");
      for( const i of xray[0]){
        // let _reference_code = JSON.parse(i.reference_code);
        
        // delete i.reference_code;
        // i.reference_code = _reference_code;

        // let _price = JSON.parse(i.price);
        // delete i.price;
        // i.price = _price;
        _data.push(i);
      }
      console.log("medicine");
      for( const i of medicine[0]){
        // let _reference_code = JSON.parse(i.reference_code);
        
        // delete i.reference_code;
        // i.reference_code = _reference_code;

        // let _price = JSON.parse(i.price);
        // delete i.price;
        // i.price = _price;
        _data.push(i);
      }
      console.log("prcd");
      for( const i of prcd[0]){
        // let _reference_code = JSON.parse(i.reference_code);
        
        // delete i.reference_code;
        // i.reference_code = _reference_code;

        // let _price = JSON.parse(i.price);
        // delete i.price;
        // i.price = _price;
        _data.push(i);
      }
      console.log("dental");
      for( const i of dental[0]){
        // let _reference_code = JSON.parse(i.reference_code);
        
        // delete i.reference_code;
        // i.reference_code = _reference_code;

        // let _price = JSON.parse(i.price);
        // delete i.price;
        // i.price = _price;
        _data.push(i);
      }
      console.log("procedureOR");
      for( const i of procedureOR[0]){
        // let _reference_code = JSON.parse(i.reference_code);
        
        // delete i.reference_code;
        // i.reference_code = _reference_code;

        // let _price = JSON.parse(i.price);
        // delete i.price;
        // i.price = _price;
        _data.push(i);
      }
      console.log("income");
      for( const i of income[0]){
        // let _reference_code = JSON.parse(i.reference_code);
        
        // delete i.reference_code;
        // i.reference_code = _reference_code;

        // let _price = JSON.parse(i.price);
        // delete i.price;
        // i.price = _price;
        _data.push(i);
      }
      
      // const info: any = await servicePaperlessModels.items(db,_data[0]);
      if(_data.length > 0) {
        for(let i of _data){
          const info: any = await servicePaperlessModels.items(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:_data
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }

    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว
  fastify.get('/ward', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.ward(db);
      if(data[0].length > 0) {
        for(let i of data[0]){
          const info: any = await servicePaperlessModels.ward(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }
    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว
  fastify.get('/medicine', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.medicine(db);
      if(data[0].length > 0) {
        for(let i of data[0]){
          const info: any = await servicePaperlessModels.medicine(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }
    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว
  fastify.get('/medicine-usage', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.medicine_usage(db);
      if(data[0].length > 0) {
        for(let i of data[0]){
          const info: any = await servicePaperlessModels.medicine_usage(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }
    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว
  fastify.get('/refer-cause', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.refer_cause(db);
      if(data[0].length > 0) {
        for(let i of data[0]){
          const info: any = await servicePaperlessModels.refer_cause(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }
    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว
  fastify.get('/refer-by', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.refer_by(db);
      if(data[0].length > 0) {
        for(let i of data[0]){
          const info: any = await servicePaperlessModels.refer_by(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }
    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });
  //ออกแล้ว
  fastify.get('/specialist', {
    // preHandler: [fastify.guard.role('ADMIN')],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    // try {
      const data: any = await serviceModels.specialist(db);
      if(data[0].length > 0) {
        for(let i of data[0]){
          const info: any = await servicePaperlessModels.specialist(pg,i);

        }
        return reply.status(StatusCodes.OK).send({
          status: 'ok',
          data:data[0]
        });
      }else{
        return reply.status(500).send({status: 'ไม่พบข้อมูล',error: getReasonPhrase(500)});
      }
    // } catch (error: any) {
    //   request.log.error(error);
    //   return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
    //     .send({
    //       status: 'error',
    //       error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
    //     });
    // }
  });

  done();

} 
